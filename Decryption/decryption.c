#include <linux/init.h>
#include <linux/kernel.h>      // Kernel work is being done here
#include <linux/module.h>      // Specifically a module
#include <linux/fs.h>          // To use struct file, struct inode, struct file_operations
#include <linux/uaccess.h>     // To use get_user and put_user

// Definitions

#define BUF_LEN 80            // Size of accepted message
#define DEVICE_NAME "decryption_char"
#define DEVICE_FILE_NAME "decryption_char"
#define MAJOR_NUM 194

static int value = 5;         // Encryption value
// static int MajorNum=0;    // Module's major number // Define the Major Number, registration will fail if already in use
// static int minorNum=0;    // Module's minor number // By default, the first is zero
static int Device_Open = 0;

static char decrypted_msg[BUF_LEN];
static char *decrypted_msg_ptr;

static int decrypt_open(struct inode *inode, struct file *file)
{
    printk(KERN_ALERT "decrypt_open\n");
    if (Device_Open)
    {
        return -EBUSY;
    }
    Device_Open++;
    decrypted_msg_ptr = decrypted_msg;
    try_module_get(THIS_MODULE);

    return 0;
}

static int decrypt_close(struct inode *inode, struct file *file)
{
    printk(KERN_ALERT "decrypt_close\n");
    Device_Open--;      /* Now that the file is closed, someone else can open it */
                        /* Decrement Device_Open to enable opening */
    module_put(THIS_MODULE);
    return 0;
}

static ssize_t decrypt_read(struct file *filp, char *buffer, size_t length, loff_t *offset)
{
    // *filp struct file, see include/linux/fs.h
    // *buffer is the buffer to fill with data
    // length is the length of the buffer

    int bytes_read = 0;     // bytes_read is defined as the number of bytes to be written to the buffer.

    if (*decrypted_msg_ptr == 0)
    {
        return 0;           // If we're at the end of the message, return 0 indicating end of file.
    }                       // No data was written to the user space buffer

    // Actually putting data into the buffer:

    while (length && *decrypted_msg_ptr) {

        /* 
           The buffer is in the user-level data segment, not the kernel level, so we can't use
           a pointer assignment; instead, we use put_user, which copies data from the kernel-level
           data segment to the user-level data segment.
        */

        put_user(*(decrypted_msg_ptr++), buffer++);

        length--;
        bytes_read++;
    }
    return bytes_read;      // Return the number of bytes written to the buffer,
                            // this is done by most read functions
}

static ssize_t decrypt_write(struct file *f, const char __user *buf, size_t len, loff_t *off)
{
    char auxbuf[BUF_LEN];
    char *auxbuf_ptr;
    int i = 0, j = 0;
    if (len > BUF_LEN)
    {
        printk(KERN_ALERT "Too many characters, only 80 available\n");
        // return -1
    }
    for (i = 0; i < len && i < BUF_LEN; i++)
    {
        get_user(auxbuf[i], buf + i);
    }
    auxbuf[len] = '\n';
    auxbuf[len + 1] = '\0';
    auxbuf_ptr = auxbuf;
    for (j = 0; j < len && j < BUF_LEN; j++)
    {
        decrypted_msg[j] = auxbuf[j] - value;
    }
    decrypted_msg[len] = '\n';
    decrypted_msg[len + 1] = '\0';
    decrypted_msg_ptr = decrypted_msg;

    return i;
}

// Assignment / actions to perform with the CDF / - / Functions of this CDD /
// CDF = Character Device File
// CDD = Character Device Driver

static struct file_operations decrypt_fops =
{
    .owner = THIS_MODULE,
    .open = decrypt_open,
    .release = decrypt_close,
    .read = decrypt_read,
    .write = decrypt_write
};

// Functions to execute when loading (init) or unloading (exit) the module

static int decrypt_init(void)
{
    int ret_val;
    ret_val = register_chrdev(MAJOR_NUM, DEVICE_NAME, &decrypt_fops);

    if (ret_val < 0)
    {
        printk(KERN_ALERT "%s failed with %d\n", "Sorry, registration of the character device", ret_val);
        return ret_val;
    }

    printk(KERN_INFO "%s The major device number is %d.\n", "Registration successful", MAJOR_NUM);
    printk(KERN_INFO "To communicate with the driver,\n");
    printk(KERN_INFO "a driver file must be created. \n");
    printk(KERN_INFO "It is suggested to use:\n");
    printk(KERN_INFO "mknod /dev/%s c %d 0\n", DEVICE_FILE_NAME, MAJOR_NUM);

    return 0;
}
static void decrypt_exit(void)
{
    unregister_chrdev(MAJOR_NUM, DEVICE_NAME);
    printk(KERN_INFO "encrypt_exit\n");
}
module_init(decrypt_init);
module_exit(decrypt_exit);

// License and Documentation

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Group: Briasco, Pardina");
MODULE_DESCRIPTION("Character driver for encryption");