# lp-linux-device-drivers

## Clone Repository

```
cd existing_repo
git remote add origin https://gitlab.com/english6060242/operating-systems/coursework/lp-linux-device-drivers.git
git branch -M main
git pull origin main
```


## Operating Systems I - Practical Work No. 3: Development of modules for Linux kernel

*Year 2019*

### Introduction
In this practical work, two simple modules will be developed for insertion into the Linux kernel. These modules will emulate the operation of two character devices. One device will perform a simple encryption of the characters written to it. The other module will perform the decryption of the characters written to it.

### Theoretical Framework
As is known, the kernel is the core of the operating system, responsible for managing system resources and performing all secure communication between software and hardware. Now, Linux kernel-based operating systems function properly with a wide variety of hardware platforms (different: video cards, sound, network, hard drives, etc). Additionally, most applications in these operating systems are developed without knowing the characteristics of the hardware on which they will be used. As the mediator between software and hardware, the kernel must contain instructions for applications to communicate with these devices. However, if the kernel were to include all existing devices, it would be extremely extensive (which is not desirable) or every time a device that is not included in the kernel at that moment is connected, the code to handle this device would need to be added to the kernel and the system restarted (as the kernel has been modified). To avoid this, kernel modules or device drivers exist.

#### What are Kernel Modules?
They are code fragments that can be loaded and unloaded from the kernel as requested. They extend the functionality of the kernel without the need to restart the system. One class of kernel module is the device driver.
Device drivers are divided into two types:
- Block Device Drivers: They contain a buffer for requests and can choose the best order to respond to them. They handle storage and network devices. They can only accept inputs and generate outputs in blocks (whose size varies depending on the device).
- Character Device Drivers (CDD): All devices that are not network or storage devices are controlled by a CDD. They can use as many bytes as desired (they are not restricted by a defined block). This is the type used in this practical work.

![Kernel_Modules](Img/Picture1.png)

For user applications to communicate with the character device driver (CDD), a device file (CDF: Character Device File) is required:
In this way, user applications communicate with the CDF, and based on the read, write, etc. operations performed on the file, the CDD will perform its function.

Device files have two associated numbers:
- Major number: Indicates which driver is used to access certain hardware (each Device Driver has a unique major number). If two or more files corresponding to devices indicate the same major number, this means they are controlled by the same driver.
- Minor number: Used to distinguish between different devices that are controlled by the same driver.
Example:

![Example_1](Img/Picture2.png)

    The case of device files for three partitions of a hard drive is observed. In the fifth and sixth columns, there are two numbers separated by a comma. The first one (3) corresponds to the major number of the driver used to handle these partitions, and since all three use the same driver, this number is repeated. The second one is different for each partition because the files must be differentiated to communicate specifically with one or the other (they are different files).

### Development
A kernel module must contain at least two functions:
- A "start" (initialization) called init_module(). This is called when a kernel module is loaded. It usually registers some driver or replaces some kernel function with its own code.
- An "end" (cleanup) called cleanup_moudle() that is called just before a kernel module is removed (rmmod). (It undoes what init_module() has done). All modules must include linux/module.h.
In coherence with this, we include this library and write the initial module with these functions:

![Code_1](Img/Picture3.png)

The register_chrdev function registers a character device with a certain major number name.
If the registration of this device fails (for example, if the major number is already occupied by other devices), it returns a negative value, in which case a message is printed and the init function exits. Also, register_chrdev has as a parameter a value of type struct file_operations which will be discussed later.
The unregister_chrdev function undoes what was done by the previous function.
    In short, our driver must indicate what actions to take when a user application interacts with the character device file, so it was necessary to indicate which functions will be executed when that file is opened, closed, read, and written to. This is achieved through the struct file_operations encrypt_fops:

![Code_2](Img/Picture4.png)

Finally, we proceeded to write these functions that will be executed when interacting with the CDF (encrypt_open, encrypt_close, encrypt_read, and encrypt_write):

![Code_3](Img/Picture5.png)

- encrypt_open and encrypt_close:

It is not desirable for the file to be opened more than once at the same time, hence the Device_Open counter (global variable). 
This is incremented when the file is opened and decremented when it is closed. In order to open the file, Device_Open must be zero.

- encrypt_read (explained in the comments):

![Code_4](Img/Picture6.png)

- encrypt_write: This is the function that performs the task requested by the instructions; it obtains what the user/ user program entered and stores it in an auxiliary buffer (which is an array of characters). Then, to each element of this array, "value" is added, which is a global variable that contains the number that will be added to each character to achieve the encryption of the entered string (we chose value = 5).

![Code_5](Img/Picture7.png)

get_user was used to obtain a simple variable from the user space (remember that we are in kernel space when programming the module).
- Similarly, the decrypt_write function is identical to this one but instead of adding a value to each character of the string, thus encrypting it, it subtracts this same value, decrypting it.
Since the two modules to be implemented are similar and only vary in terms of function names, major number, device/file name, and the content of the write function, the operation was explained with the functions of the encryption module.

### Makefile
To compile the modules, it was necessary to install the Linux headers and implement a Makefile since kernel modules need to be compiled very differently from regular user programs. Thanks to kbuild and the process of building for loadable external modules integrated into the standard kernel build mechanism, the simple Makefile below results:

![Code_6](Img/Picture8.png)

Only the "all:" and "clean" directives are necessary, but we added "insert", "test", and "remove" for practicality.

### Module in Operation
For the modules to work, the following steps must be taken:
- Compile the modules.
- Load the modules into the kernel (note that doing so executes the "init" function, thus also registering the respective device).
- Create the character device file and associate it with its minor and major numbers.
- Once completed, this process allows writing to and reading from the CDF to verify if the operation is as desired.
Below are these steps for our case:

# Compile the Modules

![Code_7](Img/Picture9.png)

Note that before executing the `make` command, the only files in the folders are the Makefile and the C code of the modules.

![Code_8](Img/Picture10.png)

All corresponding files have been generated. The modules have the extension `.ko` (we use files with this extension to load the module) instead of `.o`, which distinguishes them from conventional object files as they have a `.modinfo` section that contains additional information about where the module is stored.

- **Load the modules into the kernel:** For our `encryption.ko` module (similarly for `decryption.ko`), we use the following command (in superuser mode):

```bash
insmod ./encryption.ko
```

We were able to view which modules were loaded and verify that ours were indeed loaded using the following command:

```bash
lsmod | head
```

![Code_9](Img/Picture11.png)

- **Create the character device file and associate it with its minor and major numbers:**

![Code_10](Img/Picture12.png)

```bash
mknod /dev/encryption_char c 143 0 
mknod /dev/decryption_char c 194 0
```

We can view the existing character device files and their major and minor numbers using:

```bash
ls -l /dev/
```

or search for them by their major number (e.g., encryption -> 143) using:

```bash
ls -l /dev/ | grep 143
```

![Code_11](Img/Picture13.png)

- **Read and write the character device files to verify that the operation is as desired:** "Testing OS I TP3" was written to the character device file to verify the operation of the encryption driver. Then, the same file was read to obtain the encrypted string. This string was written to the character device file for decryption, and finally, this file was read to obtain the original string:

![Code_12](Img/Picture14.png)

## Conclusion

We have successfully completed the practical work by using the concepts acquired in the Operating Systems I course regarding kernel modules and referring to the specified bibliography. The only issue we encountered was that when writing many characters to the character device files and trying to replace them with a subsequent write with fewer characters, some of the characters remain in the buffer since it is not completely cleared with each new write (no incorrect strings are returned because when writing, the end of the character string is defined with a "\0", so the modules function correctly). We did not modify these circumstances as the modules operated as desired, and therefore, we did not consider it necessary. To observe this issue, it is proposed to uncomment the code intended for this purpose in the `encrypt_close()` and `decrypt_close()` functions.

**Note:** In order to work, the Linux headers were installed. The command used for this was:

```bash
apt search linux-headers-$(uname -r)
```

## References

- [Linux Kernel Module Programming Guide](http://tldp.org/LDP/lkmpg/2.6/html/index.html)
- [Linux Device Drivers, Third Edition](https://static.lwn.net/images/pdf/LDD3/ch03.pdf)
- [YouTube: Linux Kernel Programming](https://www.youtube.com/watch?v=S8hifIrDh-g&list=PL16941B715F5507C5&index=1)